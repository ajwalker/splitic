package test

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/ajwalker/splitic/internal/timings"
	_ "gitlab.com/ajwalker/splitic/internal/timings/gitlab"
	_ "gitlab.com/ajwalker/splitic/internal/timings/junit"

	"gitlab.com/ajwalker/splitic/internal/runner"
	"gitlab.com/ajwalker/splitic/internal/runner/flags"
)

type testCmd struct {
}

func New() *testCmd {
	c := &testCmd{}
	return c
}

func (cmd *testCmd) Name() string {
	return "test"
}

func (cmd *testCmd) Execute() error {
	provider, options := flags.Parse(os.Args[1], os.Args[2:], os.Stderr)

	// get timings
	report, err := provider.Get()
	if errors.Is(err, timings.ErrNoTimingDataAvailable) {
		// is no timing data is available, we warn, but don't exit with an error
		fmt.Fprintln(os.Stderr, "[warning]", err)
		err = nil
	}
	if err != nil {
		fmt.Fprintln(os.Stderr, "error collecting timing information:", err)
		os.Exit(1)
	}

	// run tests
	err = runner.Run(report, options, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error running tests:", err)

		if errors.Is(err, runner.ErrRunHadFailures) {
			os.Exit(options.TestFailExitCode)
		}
		os.Exit(1)
	}

	fmt.Fprintf(os.Stderr, "testing finished.\n")

	return nil
}
