// +build with_skipped

package main

import "testing"

func TestSkip(t *testing.T) {
	t.SkipNow()
}

func TestSkipWithReason(t *testing.T) {
	t.Skip("reason")
}
