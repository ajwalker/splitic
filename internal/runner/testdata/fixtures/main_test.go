package main

import (
	"fmt"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	fmt.Println("Output from main")
	os.Exit(m.Run())
}

func TestSimple(t *testing.T) {
	main()
}

func TestNested(t *testing.T) {
	t.Run("a", func(t *testing.T) {})
	t.Run("b", func(t *testing.T) {})
	t.Run("c", func(t *testing.T) {})
}
